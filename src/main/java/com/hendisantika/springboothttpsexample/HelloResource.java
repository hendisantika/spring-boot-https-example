package com.hendisantika.springboothttpsexample;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-https-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 24/08/18
 * Time: 08.40
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping("/hello")
public class HelloResource {

    @GetMapping
    public String hello() {
        return "Hello YouTube";
    }
}