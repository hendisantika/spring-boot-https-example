# Spring Boot with HTTPS

### Command to create self signed SSL Certificate JKS

```
keytool -genkey -alias https-example -storetype JKS -keyalg RSA -keysize 2048 -validity 365 -keystore https-example.jks
Secured access
```

`https://localhost/hello` - returns 'Hello YouTube'

`https://localhost:8443/hello` - returns 'Hello YouTube'

Follow this article :

https://www.thomasvitale.com/https-spring-boot-ssl-certificate/#more-1193